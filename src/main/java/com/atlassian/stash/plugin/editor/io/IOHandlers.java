package com.atlassian.stash.plugin.editor.io;

import com.atlassian.stash.scm.CommandInputHandler;
import com.atlassian.stash.scm.CommandOutputHandler;

public class IOHandlers {

    public static CommandInputHandler stringIn(String string) {
        return new StringCommandInputHandler(string);
    }

    public static CommandOutputHandler<String> stringOut() {
        return new StringCommandOutputHandler();
    }

    public static FileModeLsTreeOutputHandler fileMode(String filename) {
        return new FileModeLsTreeOutputHandler(filename);
    }

}
